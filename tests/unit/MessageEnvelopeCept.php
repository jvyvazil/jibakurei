<?php

use App\Tests\UnitTester;

$I = new UnitTester($scenario);
$I->wantTo('perform actions and see result');
$message = new \App\Message\GetFavicon(1);
$I->assertEquals($message, unserialize(serialize($message)));
