<?php

namespace App\Tests;

use App\ValueObject\Email;

class ValueObjectCest
{
    public function _before(UnitTester $I)
    {
    }

    // tests
    public function testEmailVO(UnitTester $I)
    {
        $vo = new Email('test@test.com');
        $vo2 = new Email('test@test.com');
        $vo3 = new Email('test2@test.com');

        $I->assertTrue($vo->equals($vo2));
        $I->assertFalse($vo->equals($vo3));

        $I->assertEquals('test@test.com', (string) $vo);
        $I->assertEquals('test@test.com', $vo->getValue());
    }

    public function testInvalidEmailVO(UnitTester $I)
    {
        $I->expectThrowable(\InvalidArgumentException::class, function () {
            new Email('hello');
        });
    }
}
