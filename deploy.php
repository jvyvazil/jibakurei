<?php

namespace Deployer;

require 'recipe/symfony.php';

set('bin_dir', 'bin');
set('var_dir', 'var');

// Project name
set('application', 'jibakurei');

// Project repository
set('repository', 'git@bitbucket.org:jvyvazil/jibakurei.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Shared files/dirs between deploys
add('shared_files', [
    '.env',
]);
add('shared_dirs', [
    'public/uploads',
    'var/log',
    'node_modules',
]);

// Writable dirs by web server
add('writable_dirs', [
    'public/build',
    'public/uploads',
    'public/images',
    'var/cache',
    'var/log',
]);

// Hosts

host('jibakurei.cz')
    ->user('deployer')
    //->identityFile('~/.ssh/id_jibakurei_rsa.pub', '~/.ssh/id_jibakurei_rsa')
    ->stage('production')
    ->set('deploy_path', '~/{{application}}');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

/*
 * Install assets from public dir of bundles
 */
task('deploy:assets:install', function () {
    run('{{bin/php}} {{bin/console}} assets:install {{console_options}} {{release_path}}/public');
    run('yarn --cwd {{release_path}}');
    run('yarn --cwd {{release_path}} encore production');
})->desc('Install bundle assets');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'database:migrate');
