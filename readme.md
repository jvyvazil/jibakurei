# Jibakurei

![alt text](assets/images/logo-full-small.png "Logo Title Text 1")

| URL | Přihlašovací údaje |
| ------------- |:-------------:| 
| https://www.jibakurei.cz | jibakurei-test@seznam.cz:vspj |

## Nastavení vývojového prostředí

Aplikace běží na PHP 7.3 a využívá Postgresql 9.6. Dále je potřeba funkční instalace RabbitMQ, yarn (+ node.js). Pro usnadnění vývoje jsou připraveny kontejnery pro docker, v případě použití Docker není nutné vývojové prostředí nastavovat. Všechny kontejnery lze nastartovat přes `docker-compose` tímto příkazem: 

```bash
docker-compose -f dev.yml up -d
```

### Zprovoznění aplikace

Při provozování aplikace přes docker se veškeré příkazy prefixují `docker-compose -f dev.yml exec app`, kontejner `app` obsahuje veškeré potřebné utility (yarn, composer, ...). Před prvotním spuštění docker kontejnerů je ale potřeba spustit `composer install` v rootu projektu.

- Stažení zdrojových kódů aplikace:
       
        git clone git@bitbucket.org:jvyvazil/jibakurei.git
        cd jibakurei

- Instalace PHP závislostí

        composer install
        
                
- Úprava konfigurace, editujte `.env` soubor v rootu projektu, pro Docker není potřeba upravovat

- Nyní je možné spustit docker kontejnery

        docker-compose -f dev.yml up -d
        
- Instalace js závislostí a build assetů

        yarn
        yarn encore dev --watch
        
- Spuštění aplikace (v docker kontejneru není nutné spouštět, aplikace startuje automaticky na portu 8000)

        bin/console server:run
        
- Příprava databáze

        bin/console doctrine:database:create
        bin/console doctrine:migrations:migrate
        
- Naplnění databáze daty (volitelně)

        bin/console doctrine:fixtures:load
        
- Příkazy pro provoz aplikace

    - Vytvoření uživatele 
    
            bin/console app:create-user emailova@adresa.cz
    
    - Vytvoření projektu
            
            bin/console app:create-project
            
    - Plánování kontrol (na produkci nutno spouštět periodicky)
    
            bin/console cron:schedule:checks
            
    - Zpracování fronty
    
            bin/console messenger:consume-messages            

### Docker kontejnery

Při vývoji pomocí docker kontejnerů se nastartují tyto služby

| Služba | Popis | Port |
| ------ | ----- | ---- |
| db | nastartuje Postgresql 9.6 | |
| adminer | k administraci databáze | 8080 |
| app | spouští symfony aplikaci | 8000 |
| rabbitmq | správa fronty | 8001 |
| emails | odchytávání emailů | 8025 |
