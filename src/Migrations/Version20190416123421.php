<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190416123421 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Finalized schema of database, added timestamps to database.';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE result (id UUID NOT NULL, status INT NOT NULL, http_status VARCHAR(3) NOT NULL, content TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN result.created_at IS \'the datetime of creation(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN result.updated_at IS \'the datetime of last change(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE rule_check ADD rule_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE rule_check RENAME COLUMN result TO expected');
        $this->addSql('ALTER TABLE rule_check ADD CONSTRAINT FK_F2BC8CD3744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F2BC8CD3744E0351 ON rule_check (rule_id)');
        $this->addSql('ALTER TABLE rule DROP created_at');
        $this->addSql('ALTER TABLE rule DROP updated_at');
        $this->addSql('ALTER TABLE rule DROP is_enabled');
        $this->addSql('ALTER TABLE project ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE project ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN project.created_at IS \'the datetime of creation(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN project.updated_at IS \'the datetime of last change(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "user" ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE "user" ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN "user".created_at IS \'the datetime of creation(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "user".updated_at IS \'the datetime of last change(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE result');
        $this->addSql('ALTER TABLE rule_check DROP CONSTRAINT FK_F2BC8CD3744E0351');
        $this->addSql('DROP INDEX IDX_F2BC8CD3744E0351');
        $this->addSql('ALTER TABLE rule_check DROP rule_id');
        $this->addSql('ALTER TABLE rule_check RENAME COLUMN expected TO result');
        $this->addSql('ALTER TABLE "user" DROP created_at');
        $this->addSql('ALTER TABLE "user" DROP updated_at');
        $this->addSql('ALTER TABLE project DROP created_at');
        $this->addSql('ALTER TABLE project DROP updated_at');
        $this->addSql('ALTER TABLE rule ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE rule ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE rule ADD is_enabled BOOLEAN NOT NULL');
        $this->addSql('COMMENT ON COLUMN rule.created_at IS \'the datetime of creation(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN rule.updated_at IS \'the datetime of last change(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN rule.is_enabled IS \'set this attribute to false instead of deleting\'');
    }
}
