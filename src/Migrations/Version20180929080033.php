<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Initial migration.
 */
final class Version20180929080033 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE check_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE project_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE rule_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "check" (id INT NOT NULL, rule_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3C8EAC13744E0351 ON "check" (rule_id)');
        $this->addSql('COMMENT ON COLUMN "check".created_at IS \'the datetime of creation(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "check".updated_at IS \'the datetime of last change(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE project (id INT NOT NULL, title VARCHAR(120) NOT NULL, url VARCHAR(200) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE rule (id INT NOT NULL, project_id INT DEFAULT NULL, title VARCHAR(120) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, is_enabled BOOLEAN NOT NULL, discr VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_46D8ACCC166D1F9C ON rule (project_id)');
        $this->addSql('COMMENT ON COLUMN rule.created_at IS \'the datetime of creation(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN rule.updated_at IS \'the datetime of last change(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN rule.is_enabled IS \'set this attribute to false instead of deleting\'');
        $this->addSql('CREATE TABLE rule_alive (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE "check" ADD CONSTRAINT FK_3C8EAC13744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rule ADD CONSTRAINT FK_46D8ACCC166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rule_alive ADD CONSTRAINT FK_DE097A93BF396750 FOREIGN KEY (id) REFERENCES rule (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE rule DROP CONSTRAINT FK_46D8ACCC166D1F9C');
        $this->addSql('ALTER TABLE "check" DROP CONSTRAINT FK_3C8EAC13744E0351');
        $this->addSql('ALTER TABLE rule_alive DROP CONSTRAINT FK_DE097A93BF396750');
        $this->addSql('DROP SEQUENCE check_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE project_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE rule_id_seq CASCADE');
        $this->addSql('DROP TABLE "check"');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE rule');
        $this->addSql('DROP TABLE rule_alive');
    }
}
