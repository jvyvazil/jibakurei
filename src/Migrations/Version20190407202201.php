<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190407202201 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Project, rules and checks refactored';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE examination_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE rule_check_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE rule_check (id INT NOT NULL, type VARCHAR(12) NOT NULL, comparison VARCHAR(12) NOT NULL, result VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN rule_check.created_at IS \'the datetime of creation(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN rule_check.updated_at IS \'the datetime of last change(DC2Type:datetime_immutable)\'');
        $this->addSql('DROP TABLE examination');
        $this->addSql('DROP TABLE rule_alive');
        $this->addSql('ALTER TABLE rule ADD url VARCHAR(120) NOT NULL');
        $this->addSql('ALTER TABLE rule DROP discr');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE rule_check_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE examination_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE examination (id INT NOT NULL, rule_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_ccdaabc5744e0351 ON examination (rule_id)');
        $this->addSql('COMMENT ON COLUMN examination.created_at IS \'the datetime of creation(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN examination.updated_at IS \'the datetime of last change(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE rule_alive (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE examination ADD CONSTRAINT fk_ccdaabc5744e0351 FOREIGN KEY (rule_id) REFERENCES rule (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rule_alive ADD CONSTRAINT fk_de097a93bf396750 FOREIGN KEY (id) REFERENCES rule (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE rule_check');
        $this->addSql('ALTER TABLE rule ADD discr VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE rule DROP url');
    }
}
