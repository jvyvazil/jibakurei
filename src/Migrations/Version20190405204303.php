<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190405204303 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added password for user and prepared basic db structure for projects and checks';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE check_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE examination_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE examination (id INT NOT NULL, rule_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CCDAABC5744E0351 ON examination (rule_id)');
        $this->addSql('COMMENT ON COLUMN examination.created_at IS \'the datetime of creation(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN examination.updated_at IS \'the datetime of last change(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE examination ADD CONSTRAINT FK_CCDAABC5744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE "check"');
        $this->addSql('ALTER TABLE project ADD interval INT NOT NULL');
        $this->addSql('ALTER TABLE project ADD checked_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN project.checked_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "user" ADD roles JSON NOT NULL');
        $this->addSql('ALTER TABLE "user" ADD password VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE "user" ALTER email TYPE VARCHAR(180)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE examination_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE check_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "check" (id INT NOT NULL, rule_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_3c8eac13744e0351 ON "check" (rule_id)');
        $this->addSql('COMMENT ON COLUMN "check".created_at IS \'the datetime of creation(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "check".updated_at IS \'the datetime of last change(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "check" ADD CONSTRAINT fk_3c8eac13744e0351 FOREIGN KEY (rule_id) REFERENCES rule (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE examination');
        $this->addSql('ALTER TABLE project DROP interval');
        $this->addSql('ALTER TABLE project DROP checked_at');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74');
        $this->addSql('ALTER TABLE "user" DROP roles');
        $this->addSql('ALTER TABLE "user" DROP password');
        $this->addSql('ALTER TABLE "user" ALTER email TYPE VARCHAR(255)');
    }
}
