<?php

namespace App\Repository;

use App\Entity\Project\Project;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function getProjectsForUser(UserInterface $user): array
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->where(':user MEMBER OF p.users')
            ->setParameters([
                'user' => $user,
            ]);

        return $qb->getQuery()->getResult();
    }

    public function getProjectsToCheck(): array
    {
        $qb = $this
            ->createQueryBuilder('p')
            // todo: do some magic here!
            ;

        return $qb->getQuery()->getResult();
    }

    public function hasUserAccess(User $user, int $projectId): bool
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->where(':id = p.id')
            ->andWhere(':user MEMBER OF p.users')
            ->setParameters([
                'id' => $projectId,
                'user' => $user,
            ]);

        $result = $qb->getQuery()->getResult();

        return \count($result) > 0;
    }
}
