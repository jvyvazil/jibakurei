<?php

namespace App\Repository;

use App\Entity\Project\Result;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Result|null find($id, $lockMode = null, $lockVersion = null)
 * @method Result|null findOneBy(array $criteria, array $orderBy = null)
 * @method Result[]    findAll()
 * @method Result[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class ResultRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Result::class);
    }

    public function getResultsForProject(int $project_id)
    {
        $qb = $this
            ->createQueryBuilder('result')
            ->leftJoin('result.check', 'check')
            ->leftJoin('check.rule', 'rule')
            ->leftJoin('rule.project', 'project')
            ->where('project.id = :project_id')
            ->setParameters([
                'project_id' => $project_id,
            ])
            ->orderBy('result.id', 'DESC')
            ->setMaxResults(20)
            ;

        return $qb->getQuery()->getResult();
    }
}
