<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUsersSelectArray(): array
    {
        $result = [];

        $users = $this->findAll();

        foreach ($users as $user) {
            $result[] = $user->getEmail();
        }

        return $result;
    }

    public function getUserByEmail($email): User
    {
        return $this->findOneBy(['email' => $email]);
    }

    public function findAllActive()
    {
        $qb = $this
            ->createQueryBuilder('u')
            ->where('u.password IS NOT NULL')
        ;

        return $qb->getQuery()->getResult();
    }
}
