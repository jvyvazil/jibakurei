<?php

namespace App\Message;

abstract class ProjectMessage
{
    /** @var int */
    protected $projectId;

    public function __construct(int $projectId)
    {
        $this->projectId = $projectId;
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }
}
