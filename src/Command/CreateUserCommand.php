<?php

namespace App\Command;

use App\Entity\User;
use App\ValueObject\Email;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:create-user';

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Will create a new user')
            ->addArgument('email', InputArgument::REQUIRED, 'email address of the user')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');

        try {
            $user = new User();
            $user->setEmail(new Email($email));
            $user->addRole('ROLE_USER');

            $this->em->persist($user);
            $this->em->flush();

            $io->success('User was successfully created');
        } catch (\Exception $e) {
            $io->error('Error: '.$e->getMessage());
        }
    }
}
