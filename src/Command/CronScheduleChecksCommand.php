<?php

namespace App\Command;

use App\Message\PerformCheck;
use App\Service\CheckService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

final class CronScheduleChecksCommand extends Command
{
    protected static $defaultName = 'cron:schedule:checks';

    /** @var MessageBusInterface */
    private $bus;

    /** @var CheckService */
    private $service;

    public function __construct(MessageBusInterface $bus, CheckService $service)
    {
        parent::__construct();
        $this->bus = $bus;
        $this->service = $service;
    }

    protected function configure()
    {
        $this
            ->setDescription('Schedule current checks')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $projects = $this->service->getProjectsToCheck();

        foreach ($projects as $project) {
            $this->bus->dispatch(new PerformCheck($project->getId()));
        }

        $io->success('Scheduled '.\count($projects).' checks');
    }
}
