<?php

namespace App\Command;

use App\Entity\Project\Check;
use App\Entity\Project\Project;
use App\Entity\Project\Rule;
use App\Enum\UserAgent;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\Response;

final class CreateProjectCommand extends Command
{
    protected static $defaultName = 'app:create-project';

    /** @var UserRepository */
    private $repository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(UserRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Will create a new project')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $title = $io->ask('Project name:');

        $url = $io->ask('Base project URL:');
        $interval = $io->ask('Interval of checks in seconds:', 300);

        $ua = new UserAgent();
        $agents = $ua->getList();
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion('Select User-Agent:', $agents);
        $uaSelected = $helper->ask($input, $output, $question);

        $users = $this->repository->getUsersSelectArray();
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion('Select user:', $users, 0);

        $question->setMultiselect(true);
        $selected = $helper->ask($input, $output, $question);

        $project = new Project();

        $project->setTitle($title);
        $project->setUrl($url);
        $project->setInterval($interval);
        $project->setUserAgent($uaSelected);
        foreach ($selected as $k => $v) {
            $project->addUser($this->repository->getUserByEmail($v));
        }

        $rule = new Rule();
        $rule
            ->setUrl('/')
            ->setTitle('Úvodní stránka')
        ;

        $check = new Check();
        $check
            ->setType(Check::TYPE_HTTP_STATUS)
            ->setComparison(Check::COMPARISON_EQUALS)
            ->setExpected(Response::HTTP_OK)
        ;

        $rule->addCheck($check);

        $project->addRule($rule);

        $this->em->persist($check);
        $this->em->persist($rule);
        $this->em->persist($project);
        $this->em->flush();

        $io->success('Project was successfully created.');
    }
}
