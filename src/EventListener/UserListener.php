<?php

namespace App\EventListener;

use App\Entity\User;
use App\Message\UserCreated;
use App\Message\UserForgotten;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Messenger\MessageBusInterface;

final class UserListener implements EventSubscriber
{
    /** @var MessageBusInterface */
    private $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::preUpdate,
        ];
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof User) {
            $chs = $args->getEntityChangeSet();
            if (\array_key_exists('hash', $chs) && \array_key_exists('forgotten_at', $chs) && null !== $entity->getHash()) {
                $this->bus->dispatch(new UserForgotten($entity->getId()));
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof User) {
            $this->bus->dispatch(new UserCreated($entity->getId()));
        }
    }
}
