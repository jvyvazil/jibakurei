<?php

namespace App\EventListener;

use App\Entity\Project\Project;
use App\Message\CreateScreenshot;
use App\Message\GetFavicon;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Messenger\MessageBusInterface;

final class GetAssetsForProject implements EventSubscriber
{
    /** @var MessageBusInterface */
    private $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        // todo: implement if you want to update assets for updates as well
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof Project) {
            // $em = $args->getObjectManager();
            $this->bus->dispatch(new CreateScreenshot($entity->getId()));
            $this->bus->dispatch(new GetFavicon($entity->getId()));
        }
    }
}
