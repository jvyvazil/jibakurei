<?php

namespace App\DataFixtures;

use App\Entity\Project\Check;
use App\Entity\Project\Project;
use App\Entity\Project\Rule;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

final class ProjectFixtures extends BaseFixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $project = new Project();
        $project
            ->setTitle('Testovací projekt')
            ->setUrl('https://www.jibakurei.cz')
            ->setInterval(300)
        ;

        $rule = new Rule();
        $rule
            ->setTitle('Stránka s měnícím se textem')
            ->setUrl('/test')
        ;

        $check = new Check();
        $check
            ->setType(Check::TYPE_HTTP_STATUS)
            ->setComparison(Check::COMPARISON_EQUALS)
            ->setExpected(200)
            ;
        $manager->persist($check);
        $rule->addCheck($check);

        $check = new Check();
        $check
            ->setType(Check::TYPE_CONTENT)
            ->setComparison(Check::COMPARISON_CONTAINS)
            ->setExpected('<h1>Funguje!</h1>')
        ;
        $manager->persist($check);
        $rule->addCheck($check);

        $check = new Check();
        $check
            ->setType(Check::TYPE_CONTENT)
            ->setComparison(Check::COMPARISON_DOESNT_CONTAIN)
            ->setExpected('<h1>Nefunguje!</h1>')
        ;
        $manager->persist($check);
        $rule->addCheck($check);

        $manager->persist($rule);
        $project->addRule($rule);

        $rule = new Rule();
        $rule
            ->setTitle('Stránka s chybou 404')
            ->setUrl('/test/404')
        ;

        $check = new Check();
        $check
            ->setType(Check::TYPE_HTTP_STATUS)
            ->setComparison(Check::COMPARISON_EQUALS)
            ->setExpected(200)
        ;
        $manager->persist($check);
        $rule->addCheck($check);

        $manager->persist($rule);
        $project->addRule($rule);

        $rule = new Rule();
        $rule
            ->setTitle('Stránka s chybou 500')
            ->setUrl('/test/500')
        ;

        $check = new Check();
        $check
            ->setType(Check::TYPE_HTTP_STATUS)
            ->setComparison(Check::COMPARISON_EQUALS)
            ->setExpected(200)
        ;
        $manager->persist($check);
        $rule->addCheck($check);

        $manager->persist($rule);
        $project->addRule($rule);

        $project->addUser($this->getReference('user_0'));
        $project->addUser($this->getReference('user_1'));

        $manager->persist($project);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
