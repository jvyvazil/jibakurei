<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\ValueObject\Email;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class UserFixtures extends BaseFixture
{
    private $users = [
        'vyvazil@student.vspj.cz' => 'vspj',
        'jibakurei-test@seznam.cz' => 'vspj',
    ];

    /** @var UserPasswordEncoderInterface */
    private $encoder;

    public function __construct(ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct($manager);
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $it = 0;
        foreach ($this->users as $email => $password) {
            $user = new User();
            $user
                ->setEmail(new Email($email))
                ->addRole('ROLE_USER')
            ;

            $user->setPassword($this->encoder->encodePassword($user, $password));

            $manager->persist($user);
            $this->setReference('user_'.$it++, $user);
        }

        $manager->flush();
    }
}
