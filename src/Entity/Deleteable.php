<?php

namespace App\Entity;

interface Deleteable
{
    public function isEnabled(): bool;

    public function setEnabled(bool $enabled);

    public function getDeletedAt(): ? \DateTimeImmutable;
}
