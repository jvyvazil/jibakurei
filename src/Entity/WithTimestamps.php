<?php

namespace App\Entity;

interface WithTimestamps
{
    public function getCreatedAt(): \DateTimeImmutable;

    public function getUpdatedAt(): \DateTimeImmutable;
}
