<?php

namespace App\Entity\Project;

use App\Entity\Attribute\Identifier;
use App\Entity\Attribute\Timestamps;
use App\Entity\BaseEntity;
use App\Entity\WithTimestamps;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CheckRepository")
 * @ORM\Table(name="rule_check")
 * @JMS\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
class Check extends BaseEntity implements WithTimestamps
{
    use Identifier, Timestamps;

    const TYPE_HTTP_STATUS = 'http-status';
    const TYPE_CONTENT = 'content';

    const COMPARISON_EQUALS = 'equals';
    const COMPARISON_CONTAINS = 'contains';
    const COMPARISON_DOESNT_CONTAIN = '!contains';

    /**
     * @var Rule
     * @ORM\ManyToOne(targetEntity="App\Entity\Project\Rule", inversedBy="checks")
     * @ORM\JoinColumn(name="rule_id", referencedColumnName="id")
     */
    private $rule;

    /**
     * @ORM\Column(name="type", type="string", length=12)
     */
    private $type;

    /**
     * @ORM\Column(name="comparison", type="string", length=12)
     */
    private $comparison;

    /**
     * @ORM\Column(name="expected", type="string")
     */
    private $expected;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Project\Result", mappedBy="check", cascade={"remove"})
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $results;

    public function __construct()
    {
        $this->results = new ArrayCollection();
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getTypeText(): string
    {
        switch ($this->type) {
            case self::TYPE_HTTP_STATUS:
                return 'HTTP status';
            default:
                return 'Text na stránce';
        }
    }

    public function setType($type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getComparison(): string
    {
        return $this->comparison;
    }

    public function getComparisonText(): string
    {
        switch ($this->comparison) {
            case self::COMPARISON_DOESNT_CONTAIN:
                return 'neobsahuje';
            case self::COMPARISON_CONTAINS:
                return 'obsahuje';
            default:
                return '=';
        }
    }

    public function setComparison($comparison): self
    {
        $this->comparison = $comparison;

        return $this;
    }

    public function getExpected(): string
    {
        return $this->expected;
    }

    public function setExpected($expected): self
    {
        $this->expected = $expected;

        return $this;
    }

    public function getRule(): Rule
    {
        return $this->rule;
    }

    public function setRule(Rule $rule): self
    {
        $this->rule = $rule;

        return $this;
    }

    public function getLastResult(): ? Result
    {
        return $this->results->first() ?: null;
    }

    public function getStatus(): int
    {
        if ($r = $this->getLastResult()) {
            return $r->getStatus();
        }

        return Result::STATUS_NO_DATA;
    }

    public function getLastCheckAt(): ? \DateTimeImmutable
    {
        return ($result = $this->getLastResult()) ? $result->getCreatedAt() : null;
    }

    public function getConsecutiveFails(): int
    {
        $results = $this->results;

        /** @var Result $last */
        $result = $results->first();

        if (!$result) {
            return 0;
        }

        $count = 0;

        while (null !== $result && Result::STATUS_FAILING === $result->getStatus()) {
            ++$count;
            $index = $results->indexOf($result);
            $result = $results->get($index + 1);
        }

        unset($results);

        return $count;
    }
}
