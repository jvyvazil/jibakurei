<?php

namespace App\Entity\Project;

use App\Entity\Attribute\Identifier;
use App\Entity\Attribute\Timestamps;
use App\Entity\BaseEntity;
use App\Entity\WithTimestamps;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultRepository")
 * @JMS\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
class Result extends BaseEntity implements WithTimestamps
{
    use Identifier, Timestamps;

    const STATUS_FAILING = 0;
    const STATUS_OK = 1;
    const STATUS_NO_DATA = 2;
    const STATUS_NO_CHECKS = 3;
    const STATUS_NO_RULES = 4;

    private $statuses = [
        0 => 'Chyba',
        1 => 'OK',
        2 => 'Nezkontrolováno',
        3 => 'Žádné kontroly',
        4 => 'Žádní hlídači',
    ];

    /**
     * @var Check
     * @ORM\ManyToOne(targetEntity="Check", inversedBy="results")
     * @ORM\JoinColumn(name="rule_check_id", referencedColumnName="id")
     */
    private $check;

    /**
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @ORM\Column(name="http_status", type="string", length=3)
     */
    private $httpStatus;

    /**
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    public function getCheck(): Check
    {
        return $this->check;
    }

    public function setCheck(Check $check): self
    {
        $this->check = $check;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status ? self::STATUS_OK : self::STATUS_FAILING;
    }

    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatusText(): string
    {
        return $this->statuses[$this->getStatus()];
    }

    public function getHttpStatus(): ? string
    {
        return $this->httpStatus;
    }

    public function setHttpStatus($httpStatus): self
    {
        $this->httpStatus = $httpStatus;

        return $this;
    }

    public function getContent(): ? string
    {
        return $this->content;
    }

    public function setContent($content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIncidentText(): string
    {
        if (self::STATUS_FAILING !== $this->getStatus()) {
            return '';
        }

        $check = $this->getCheck();
        if (Check::TYPE_HTTP_STATUS === $check->getType()) {
            return $this->getHttpStatus();
        }

        switch ($check->getType()) {
            case Check::COMPARISON_CONTAINS:
                return 'text nenalezen';
            case Check::COMPARISON_EQUALS:
                return 'text se neshoduje';
            case Check::COMPARISON_DOESNT_CONTAIN:
                return 'text nalezen';
        }

        return '';
    }
}
