<?php

namespace App\Entity\Project;

use App\Entity\Attribute\Identifier;
use App\Entity\Attribute\Timestamps;
use App\Entity\BaseEntity;
use App\Entity\User;
use App\Entity\WithTimestamps;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 * @JMS\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
class Project extends BaseEntity implements WithTimestamps
{
    use Identifier, Timestamps;

    /**
     * @ORM\Column(type="string", length=120)
     * @JMS\SerializedName("title")
     */
    private $title;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Project\Rule", mappedBy="project", cascade={"remove"})
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $rules;

    /**
     * @var string
     * @Assert\Url
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $url;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="projects")
     * @ORM\JoinTable(name="user_x_project")
     */
    private $users;

    /**
     * Interval of checks for this project.
     *
     * @var int
     * @ORM\Column(type="integer")
     */
    private $interval;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(name="checked_at", type="datetime_immutable", nullable=true)
     */
    private $checkedAt;

    /**
     * @ORM\Column(name="user_agent", type="string", length=12, options={"default" = "bot"})
     */
    private $userAgent;

    /**
     * @ORM\Column(name="favicon", type="string", length=120, nullable=true)
     */
    private $favicon;

    /**
     * @ORM\Column(name="screenshot", type="string", length=120, nullable=true)
     */
    private $screenshot;

    public function __construct()
    {
        $this->rules = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ? string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function addUser(UserInterface $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }

        return $this;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addRule(Rule $rule): self
    {
        if (!$this->rules->contains($rule)) {
            $rule->setProject($this);
            $this->rules->add($rule);
        }

        return $this;
    }

    public function getRules(): Collection
    {
        return $this->rules;
    }

    public function getInterval(): int
    {
        return $this->interval;
    }

    public function getComputedInterval(int $fails = null): int
    {
        if (null === $fails) {
            $fails = $this->countConsecutiveFails();
        }

        $x = $this->getInterval();

        switch ($fails) {
            case 1:
                return $this->downsizeInterval($x, 1);
            case 2:
                return $this->downsizeInterval($x, 2);
            default:
                return $x;
        }
    }

    public function setInterval(int $interval): void
    {
        $this->interval = $interval;
    }

    public function getCheckedAt(): ? \DateTimeImmutable
    {
        return $this->checkedAt;
    }

    public function setCheckedAt(\DateTimeImmutable $checkedAt): void
    {
        $this->checkedAt = $checkedAt;
    }

    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    public function setUserAgent(string $userAgent): self
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    public function getFavicon(): ? string
    {
        return $this->favicon;
    }

    public function setFavicon(string $favicon): self
    {
        $this->favicon = $favicon;

        return $this;
    }

    public function getScreenshot(): ? string
    {
        return $this->screenshot;
    }

    public function setScreenshot(string $screenshot): self
    {
        $this->screenshot = $screenshot;

        return $this;
    }

    public function getStatus(): int
    {
        if (0 === \count($this->getRules())) {
            return Result::STATUS_NO_RULES;
        }

        $status = Result::STATUS_NO_DATA;

        foreach ($this->getLastResults() as $result) {
            if (Result::STATUS_FAILING === $result->getStatus()) {
                return Result::STATUS_FAILING;
            }

            if (Result::STATUS_OK !== $status) {
                $status = $result->getStatus();
            }

            if (Result::STATUS_OK === $result->getStatus()) {
                $status = Result::STATUS_OK;
            }
        }

        return $status;
    }

    public function getLastResults(): ArrayCollection
    {
        $results = new ArrayCollection();

        foreach ($this->getRules() as $rule) {
            if (!$results->isEmpty()) {
                foreach ($rule->getLastResults() as $r) {
                    $results->add($r);
                }
            } else {
                $results = $rule->getLastResults();
            }
        }

        return $results;
    }

    public function countConsecutiveFails(): int
    {
        if (Result::STATUS_FAILING !== $this->getStatus()) {
            return 0;
        }

        $failed = $this->getLastResults()->filter(function ($entry) { /* @var Result $entry */
            return Result::STATUS_FAILING === $entry->getStatus();
        });

        if ($failed->isEmpty()) {
            return 0;
        }

        $cf = 0;

        /** @var Result $f */
        foreach ($failed as $f) {
            $check = $f->getCheck();
            $ccf = $check->getConsecutiveFails();

            if ($ccf > $cf) {
                $cf = $ccf;
            }
        }

        return $cf;
    }

    public function getEmailAddressesOfUsers(): array
    {
        $emails = [];

        /** @var User $user */
        foreach ($this->getUsers() as $user) {
            array_push($emails, $user->getEmail()->getValue());
        }

        return $emails;
    }

    public function lastIncidentAt(): ? \DateTimeImmutable
    {
        return null;
    }

    public function getLastCheckAt(): ? \DateTimeImmutable
    {
        if (($results = $this->getLastResults()) && !$results->isEmpty()) {
            return $results->first()->getCreatedAt();
        }

        return null;
    }

    private function downsizeInterval(int $x, int $times): int
    {
        if ($times > 1) {
            $x = $this->downsizeInterval($x, $times - 1);
        }

        return (int) ((int) $x / ((log($x, 2)) + 1));
    }
}
