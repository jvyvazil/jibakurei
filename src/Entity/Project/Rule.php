<?php

namespace App\Entity\Project;

use App\Entity\Attribute\Identifier;
use App\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RuleRepository")
 * @JMS\ExclusionPolicy("all")
 */
class Rule extends BaseEntity
{
    use Identifier;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $title;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="App\Entity\Project\Project", inversedBy="rules")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    private $project;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Project\Check", mappedBy="rule", cascade={"remove"})
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $checks;

    public function __construct()
    {
        $this->checks = new ArrayCollection();
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function addCheck(Check $check): self
    {
        if (!$this->checks->contains($check)) {
            $check->setRule($this);
            $this->checks->add($check);
        }

        return $this;
    }

    public function getChecks(): Collection
    {
        return $this->checks;
    }

    public function removeCheck(Check $check): self
    {
        if ($this->checks->contains($check)) {
            $this->checks->removeElement($check);
        }

        return $this;
    }

    public function getProject(): Project
    {
        return $this->project;
    }

    public function setProject(Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl($url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getStatus(): int
    {
        if (0 === \count($this->getChecks())) {
            return Result::STATUS_NO_CHECKS;
        }

        $status = Result::STATUS_NO_DATA;

        foreach ($this->getChecks() as $check) {
            if (Result::STATUS_FAILING === $check->getStatus()) {
                return Result::STATUS_FAILING;
            }

            if (Result::STATUS_OK === $check->getStatus()) {
                $status = Result::STATUS_OK;
            }
        }

        return $status;
    }

    public function getLastResults(): ArrayCollection
    {
        $result = new ArrayCollection();

        foreach ($this->getChecks() as $check) {
            if ($check->getLastResult()) {
                $result->add($check->getLastResult());
            }
        }

        return $result;
    }

    public function getLastCheckAt(): ? \DateTimeImmutable
    {
        $results = $this->getLastResults();

        if ($results->isEmpty()) {
            return null;
        }

        $lastCheck = null;
        foreach ($results as $result) {
            $d = $result->getCreatedAt();
            if (null === $lastCheck) {
                $lastCheck = $d;
            } else {
                if ($lastCheck < $d) {
                    $lastCheck = $d;
                }
            }
        }

        return $lastCheck;
    }
}
