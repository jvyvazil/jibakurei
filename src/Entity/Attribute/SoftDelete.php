<?php

namespace App\Entity\Attribute;

trait SoftDelete
{
    /**
     * @ORM\Column(name="is_enabled", type="boolean", options={"comment":"set this attribute to false instead of deleting"})
     * @JMS\Expose()
     * @JMS\Groups({"enabled"})
     */
    protected $enabled = true;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(name="deleted_at", type="datetime_immutable", options={"comment":"the datetime of soft-deleting"}, nullable=true)
     * @JMS\Expose()
     */
    protected $deletedAt;

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        $enabled ? $this->deletedAt = null : $this->deletedAt = new \DateTimeImmutable();

        return $this;
    }

    public function getDeletedAt(): ? \DateTimeImmutable
    {
        return $this->deletedAt;
    }
}
