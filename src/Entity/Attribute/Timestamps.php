<?php

namespace App\Entity\Attribute;

trait Timestamps
{
    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(name="created_at", type="datetime_immutable", options={"comment":"the datetime of creation"})
     * @JMS\Expose()
     * @JMS\Groups({"created"})
     */
    protected $createdAt;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(name="updated_at", type="datetime_immutable", options={"comment":"the datetime of last change"}, nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"updated-at"})
     */
    protected $updatedAt;

    /** @ORM\PrePersist */
    public function saveChangeDatetimeOnPrePersist(): void
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTimeImmutable();
        }
        $this->updatedAt = new \DateTimeImmutable();
    }

    /** @ORM\PreUpdate */
    public function saveChangeDatetimeOnUpdate(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }
}
