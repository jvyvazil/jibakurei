<?php

namespace App\Entity\Attribute;

trait UUIdentifier
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    public function __clone()
    {
        $this->id = null;
    }

    final public function getId(): string
    {
        return $this->id;
    }
}
