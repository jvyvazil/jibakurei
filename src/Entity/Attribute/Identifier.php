<?php

namespace App\Entity\Attribute;

use JMS\Serializer\Annotation as JMS;

trait Identifier
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\SerializedName("id")
     * @JMS\Expose()
     */
    private $id;

    public function getId(): ? int
    {
        return $this->id;
    }
}
