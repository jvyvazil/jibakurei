<?php

namespace App\Entity;

use App\Entity\Attribute\Identifier;
use App\Entity\Attribute\Timestamps;
use App\Entity\Project\Project;
use App\ValueObject\Email;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 * @JMS\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseEntity implements UserInterface, WithTimestamps
{
    use Identifier, Timestamps;

    /**
     * Email is used as an username for login.
     *
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $roles;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Project\Project", mappedBy="users")
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $projects;

    /**
     * The hash is used for setting up the password and recreating password (this depends on datetime in forgotten_at field).
     *
     * @var string|null
     * @ORM\Column(name="hash", type="string", length=64, nullable=true)
     */
    private $hash;

    /**
     * @var \DateTimeImmutable|null
     * @ORM\Column(name="forgotten_at", type="datetime_immutable", nullable=true)
     */
    private $forgotten_at;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $project->addUser($this);
            $this->projects->add($project);
        }

        return $this;
    }

    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function getEmail(): ? Email
    {
        return $this->email ? new Email($this->email) : null;
    }

    public function setEmail(Email $email): self
    {
        $this->email = $email->getValue();

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole(string $role): self
    {
        if (null === $this->roles) {
            $this->roles[] = $role;

            return $this;
        }

        if (!\in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt()
    {
        // not needed since the app is using argon2i
    }

    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * Removes sensitive data from the user.
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getHash(): ? string
    {
        return $this->hash;
    }

    public function generateHash(): self
    {
        $this->hash = hash('sha256', bin2hex(random_bytes(20)));

        return $this;
    }

    public function generateHashWithDatetime(): self
    {
        $this->generateHash();
        $this->forgotten_at = new \DateTimeImmutable();

        return $this;
    }

    public function getForgottenAt(): ? \DateTimeImmutable
    {
        return $this->forgotten_at;
    }

    public function clearHash(): self
    {
        $this->hash = null;
        $this->forgotten_at = null;

        return $this;
    }
}
