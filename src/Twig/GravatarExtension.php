<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

final class GravatarExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('gravatar', [$this, 'gravatarFilter']),
        ];
    }

    public function gravatarFilter($email, $size = null, $default = null): string
    {
        $defaults = [
            '404',
            'mm',
            'identicon',
            'monsterid',
            'wavatar',
            'retro',
            'blank',
        ];
        $hash = md5($email);
        $url = 'https://www.gravatar.com/avatar/'.$hash;

        // Size
        if (null !== $size) {
            $url .= "?s=$size";
        }

        // Default
        if (null !== $default) {
            $url .= null === $size ? '?' : '&';
            $url .= \in_array($default, $defaults, true) ? $default : urlencode($default);
        }

        return $url;
    }
}
