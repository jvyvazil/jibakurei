<?php

namespace App\Mapper;

use App\Entity\Project\Check;
use App\Entity\Project\Project;
use App\Entity\Project\Result;
use App\Entity\Project\Rule;
use App\Entity\User;

final class ProjectMapper
{
    /** @var Project[] $projects */
    public function all(array $projects): array
    {
        $result = [];

        foreach ($projects as $project) {
            $checks = 0;
            foreach ($project->getRules() as $r) {
                $checks += \count($r->getChecks());
            }

            $result[] = [
                'id' => $project->getId(),
                'title' => $project->getTitle(),
                'url' => $project->getUrl(),
                'favicon' => $project->getFavicon(),
                'screenshot' => $project->getScreenshot(),
                'interval' => $project->getInterval(),
                'checks_count' => $checks,
                'rules_count' => \count($project->getRules()),
                'status' => $project->getStatus(),
                'last_incident' => $project->lastIncidentAt(),
                'last_check_at' => $project->getCheckedAt() ? $project->getCheckedAt()->format('c') : null,
            ];
        }

        return $result;
    }

    public function get(Project $project): array
    {
        $rules = [];

        /** @var Rule $rule */
        foreach ($project->getRules() as $rule) {
            $rules[] = [
                'id' => $rule->getId(),
                'title' => $rule->getTitle(),
                'url' => $rule->getUrl(),
                'status' => $rule->getStatus(),
                'last_check_at' => $rule->getLastCheckAt() ? $rule->getLastCheckAt()->format('c') : null,
            ];
        }

        return [
            'id' => $project->getId(),
            'title' => $project->getTitle(),
            'created_at' => $project->getCreatedAt(),
            'url' => $project->getUrl(),
            'favicon' => $project->getFavicon(),
            'screenshot' => $project->getScreenshot(),
            'interval' => $project->getInterval(),
            'last_check_at' => $project->getCheckedAt() ? $project->getCheckedAt()->format('c') : null,
            'last_incident' => $project->lastIncidentAt(),
            'is_ok' => Result::STATUS_OK === $project->getStatus(),
            'rules' => $rules,
            'status' => $project->getStatus(),
        ];
    }

    public function users(Project $project, User $user): array
    {
        $result = [];
        $users = $project->getUsers();

        /** @var User $u */
        foreach ($users as $u) {
            $result[] = [
                'id' => $u->getId(),
                'email' => $u->getEmail()->getValue(),
                'me' => $u->getId() === $user->getId(),
            ];
        }

        return $result;
    }

    public function checks(Rule $rule): array
    {
        $result = [];

        /** @var Check $check */
        foreach ($rule->getChecks() as $check) {
            $result[] = [
                'id' => $check->getId(),
                'type' => $check->getType(),
                'comparison' => $check->getComparison(),
                'expected' => $check->getExpected(),
                'status' => $check->getStatus(),
                'last_check_at' => $check->getLastCheckAt() ? $check->getLastCheckAt()->format('c') : null,
            ];
        }

        return $result;
    }

    public function results($results, $getUser)
    {
        $result = [];

        /** @var Result $r */
        foreach ($results as $r) {
            $result[] = [
                'id' => $r->getId(),
                'created_at' => $r->getCreatedAt()->format('c'),
                'status' => $r->getStatus(),
                'url' => $r->getCheck()->getRule()->getUrl(),
                'description' => $r->getCheck()->getTypeText().' '.$r->getCheck()->getComparisonText().' '.$r->getCheck()->getExpected(),
                'value' => $r->getIncidentText(),
            ];
        }

        return $result;
    }
}
