<?php

namespace App\Mapper;

use App\Entity\User;

final class UserMapper
{
    public function all(array $users, User $user): array
    {
        $result = [];

        /** @var User $u */
        foreach ($users as $u) {
            if ($u->getId() !== $user->getId()) {
                $result[] = [
                    'id' => $u->getId(),
                    'email' => $u->getEmail()->getValue(),
                ];
            }
        }

        return $result;
    }
}
