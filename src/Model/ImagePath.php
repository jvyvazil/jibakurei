<?php

namespace App\Model;

final class ImagePath
{
    private $dir;
    private $filename;

    public function __construct(string $dir, string $filename)
    {
        $this->dir = $dir;
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getDir(): string
    {
        return $this->dir;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getPath(): string
    {
        return $this->dir.'/'.$this->filename;
    }
}
