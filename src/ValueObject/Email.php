<?php

namespace App\ValueObject;

final class Email
{
    private $value;

    public function __construct(string $email)
    {
        $email = trim($email);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException('Email has to be a valid email address');
        }
        $this->value = $email;
    }

    public function __toString()
    {
        return $this->value;
    }

    public function equals(Email $email): bool
    {
        return $this->value === $email->value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
