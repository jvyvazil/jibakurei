<?php

namespace App\MessageHandler;

use App\Message\IncidentNotification;
use App\Repository\ProjectRepository;
use App\Service\EmailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class IncidentHandler implements MessageHandlerInterface
{
    /** @var EmailService */
    private $emailService;

    /** @var ProjectRepository */
    private $repository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EmailService $emailService, ProjectRepository $repository, EntityManagerInterface $em)
    {
        $this->emailService = $emailService;
        $this->repository = $repository;
        $this->em = $em;
    }

    public function __invoke(IncidentNotification $message)
    {
        $this->em->clear();
        $project = $this->repository->find($message->getProjectId());

        $this->emailService->sendEmailAboutIncident($project);
    }
}
