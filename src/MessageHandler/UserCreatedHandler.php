<?php

namespace App\MessageHandler;

use App\Message\UserCreated;
use App\Repository\UserRepository;
use App\Service\EmailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UserCreatedHandler implements MessageHandlerInterface
{
    /** @var EmailService */
    private $emailService;

    /** @var UserRepository */
    private $repository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EmailService $emailService, UserRepository $repository, EntityManagerInterface $em)
    {
        $this->emailService = $emailService;
        $this->repository = $repository;
        $this->em = $em;
    }

    public function __invoke(UserCreated $message)
    {
        $this->em->clear();
        $id = $message->getUserId();
        $user = $this->repository->find($id);

        $user->generateHash();
        $this->em->persist($user);
        $this->em->flush();

        $this->emailService->sendUserCreatedEmail($user);
    }
}
