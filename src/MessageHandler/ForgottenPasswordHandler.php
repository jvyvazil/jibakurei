<?php

namespace App\MessageHandler;

use App\Message\UserForgotten;
use App\Repository\UserRepository;
use App\Service\EmailService;
use Doctrine\ORM\EntityManagerInterface;

final class ForgottenPasswordHandler
{
    /** @var EmailService */
    private $emailService;

    /** @var UserRepository */
    private $repository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EmailService $emailService, UserRepository $repository, EntityManagerInterface $em)
    {
        $this->emailService = $emailService;
        $this->repository = $repository;
        $this->em = $em;
    }

    public function __invoke(UserForgotten $message)
    {
        $this->em->clear();
        $id = $message->getUserId();
        $user = $this->repository->find($id);
        $this->emailService->sendForgottenPasswordEmail($user);
    }
}
