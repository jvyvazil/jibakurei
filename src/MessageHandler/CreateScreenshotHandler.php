<?php

namespace App\MessageHandler;

use App\Message\CreateScreenshot;
use App\Repository\ProjectRepository;
use App\Service\ScreenshotService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateScreenshotHandler implements MessageHandlerInterface
{
    /** @var ScreenshotService */
    private $service;

    /** @var ProjectRepository */
    private $repository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(ScreenshotService $service, ProjectRepository $repository, EntityManagerInterface $em)
    {
        $this->service = $service;
        $this->repository = $repository;
        $this->em = $em;
    }

    public function __invoke(CreateScreenshot $message): void
    {
        $this->em->clear();
        $project = $this->repository->find($message->getProjectId());

        if (!$project) {
            return;
        }

        $path = $this->service->create($project->getUrl());

        if (null !== $path) {
            $project->setScreenshot($path);

            $this->em->persist($project);
            $this->em->flush();
        }
    }
}
