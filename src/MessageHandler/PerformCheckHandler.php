<?php

namespace App\MessageHandler;

use App\Message\IncidentNotification;
use App\Message\PerformCheck;
use App\Repository\ProjectRepository;
use App\Service\CheckService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class PerformCheckHandler implements MessageHandlerInterface
{
    /** @var ProjectRepository */
    private $repository;

    /** @var CheckService */
    private $service;

    /** @var MessageBusInterface */
    private $bus;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(ProjectRepository $repository, CheckService $service, MessageBusInterface $bus, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->bus = $bus;
        $this->em = $em;
    }

    public function __invoke(PerformCheck $examination): void
    {
        $this->em->clear();
        $project = $this->repository->find($examination->getId());

        if (!$project) {
            return;
        }

        $this->service->performChecks($project);
        $this->service->updateLastCheck($project);

        if (2 === $project->countConsecutiveFails()) {
            $this->bus->dispatch(new IncidentNotification($project->getId()));
        }
    }
}
