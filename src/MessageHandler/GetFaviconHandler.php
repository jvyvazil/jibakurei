<?php

namespace App\MessageHandler;

use App\Message\GetFavicon;
use App\Repository\ProjectRepository;
use App\Service\FaviconDownloader;
use Doctrine\ORM\EntityManagerInterface;

final class GetFaviconHandler
{
    /** @var FaviconDownloader */
    private $service;

    /** @var ProjectRepository */
    private $repository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(FaviconDownloader $service, ProjectRepository $repository, EntityManagerInterface $em)
    {
        $this->service = $service;
        $this->repository = $repository;
        $this->em = $em;
    }

    public function __invoke(GetFavicon $message): void
    {
        $project = $this->repository->find($message->getProjectId());

        if (!$project) {
            return;
        }

        $path = $this->service->getFavicon($project->getUrl());

        if (null !== $path) {
            $project->setFavicon($path);

            $this->em->persist($project);
            $this->em->flush();
        }
    }
}
