<?php

namespace App\Form\Entity;

use Symfony\Component\Validator\Constraints as Assert;

final class Password
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $password;
}
