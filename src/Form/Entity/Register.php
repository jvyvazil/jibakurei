<?php

namespace App\Form\Entity;

use App\Entity\User;
use App\ValueObject\Email;
use Symfony\Component\Validator\Constraints as Assert;

final class Register
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "Email '{{ value }}' není validní.",
     *     checkMX = false
     *     )
     */
    public $email;

    public function convertToUser(): User
    {
        $user = new User();
        $user
            ->setEmail(new Email($this->email))
            ->addRole('ROLE_USER')
        ;

        return $user;
    }
}
