<?php

namespace App\Controller\API;

use App\Entity\Project\Check;
use App\Entity\Project\Project;
use App\Entity\Project\Rule;
use App\Mapper\ProjectMapper;
use App\Repository\CheckRepository;
use App\Repository\ProjectRepository;
use App\Repository\ResultRepository;
use App\Repository\RuleRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/** @Route("/api/projects", name="api_project_") */
final class ProjectController extends ApiController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var Security */
    private $security;

    /** @var ProjectMapper */
    private $mapper;

    public function __construct(EntityManagerInterface $em, Security $security, ProjectMapper $mapper)
    {
        $this->em = $em;
        $this->security = $security;
        $this->mapper = $mapper;
    }

    /**
     * @Route("/", name="list", methods={"GET"})
     */
    public function list(ProjectRepository $repository): Response
    {
        $projects = $repository->getProjectsForUser($this->security->getUser());

        return new JsonResponse($this->mapper->all($projects));
    }

    /**
     * @Route("/{id}", name="detail", methods={"GET"})
     */
    public function detail(string $id, ProjectRepository $repository): Response
    {
        $project = $repository->find($id);

        if (!$project || !$project->getUsers()->contains($this->security->getUser())) {
            return $this->returnNotFound('Project was not found');
        }

        return new JsonResponse($this->mapper->get($project));
    }

    /**
     * @Route("/{id}/users", name="add_user", methods={"POST"})
     */
    public function addUser(int $id, Request $request, UserRepository $userRepository, ProjectRepository $projectRepository, EntityManagerInterface $em): Response
    {
        $data = json_decode($request->getContent(), true);

        if (!\array_key_exists('user_id', $data)) {
            return $this->returnPreconditionFailed('Missing attribute `user_id` in body of the request');
        }

        $userId = $data['user_id'];
        $user = $userRepository->find($userId);

        if (!$user) {
            return $this->returnNotFound('User with ID `'.$userId.'` was not found.`');
        }

        $project = $projectRepository->find($id);

        if (!$project) {
            return $this->returnNotFound('Project was not found');
        }

        $project->addUser($user);
        // todo: reconsider if notification should be sent?

        $em->persist($project);
        $em->flush();

        return new JsonResponse([], Response::HTTP_CREATED);
    }

    /**
     * @Route("/{id}/users", name="list_users", methods={"GET"})
     */
    public function listUsers(int $id, ProjectRepository $repository)
    {
        $project = $repository->find($id);

        return new JsonResponse($this->mapper->users($project, $this->getUser()));
    }

    /** @Route("/{id}/rules", name="add_rule", methods={"POST"}) */
    public function addRule(int $id, Request $request, ProjectRepository $repository, EntityManagerInterface $em): Response
    {
        $data = json_decode($request->getContent(), true);

        if (!\array_key_exists('title', $data)) {
            return $this->returnPreconditionFailed('Missing attribute `title` in body of the request');
        }

        if (!\array_key_exists('url', $data)) {
            return $this->returnPreconditionFailed('Missing attribute `url` in body of the request');
        }

        $project = $repository->find($id);

        if (!$project) {
            return $this->returnNotFound('Project was not found');
        }

        $rule = new Rule();
        $rule
            ->setTitle($data['title'])
            ->setUrl($data['url'])
            ;

        $project->addRule($rule);

        $em->persist($rule);
        $em->persist($project);
        $em->flush();

        return new JsonResponse([], Response::HTTP_CREATED, ['rule_id' => $rule->getId()]);
    }

    /** @Route("/{project_id}/rules/{rule_id}", name="delete_rule", methods={"DELETE"}) */
    public function deleteRule(int $project_id, int $rule_id, RuleRepository $repository, EntityManagerInterface $em): Response
    {
        $rule = $repository->find($rule_id);

        if ($rule->getProject()->getId() !== $project_id) {
            $rule = null;
        }

        if (!$rule) {
            return $this->returnNotFound('Rule was not found');
        }

        $em->remove($rule);
        $em->flush();

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }

    /** @Route("/{project_id}/rules/{rule_id}/checks", name="add_check", methods={"POST"}) */
    public function addCheck(int $project_id, int $rule_id, Request $request, RuleRepository $repository, EntityManagerInterface $em): Response
    {
        $data = json_decode($request->getContent(), true);

        if (!\array_key_exists('type', $data)) {
            return $this->returnPreconditionFailed('Missing attribute `type` in body of the request');
        }

        if (!\array_key_exists('comparison', $data)) {
            return $this->returnPreconditionFailed('Missing attribute `comparison` in body of the request');
        }

        if (!\array_key_exists('expected', $data)) {
            return $this->returnPreconditionFailed('Missing attribute `expected` in body of the request');
        }

        $rule = $repository->find($rule_id);

        if ($rule->getProject()->getId() !== $project_id) {
            $rule = null;
        }

        if (!$rule) {
            return $this->returnNotFound('Rule was not found');
        }

        $check = new Check();
        $check
            ->setType($data['type'])
            ->setComparison($data['comparison'])
            ->setExpected($data['expected'])
            ;

        $rule->addCheck($check);

        $em->persist($check);
        $em->persist($rule);
        $em->flush();

        return new JsonResponse([], Response::HTTP_CREATED, ['check_id' => $check->getId()]);
    }

    /** @Route("/{project_id}/results", name="results", methods={"GET"}) */
    public function getLastChecks(int $project_id, ResultRepository $repository): Response
    {
        $results = $repository->getResultsForProject($project_id);

        return new JsonResponse($this->mapper->results($results, $this->getUser()));
    }

    /** @Route("/{project_id}/rules/{rule_id}/checks/{check_id}", name="delete_check", methods={"DELETE"}) */
    public function deleteCheck(int $project_id, int $rule_id, int $check_id, CheckRepository $repository, EntityManagerInterface $em): Response
    {
        $check = $repository->find($check_id);

        $em->remove($check);
        $em->flush();

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }

    /** @Route("/{project_id}/rules/{rule_id}/checks", name="list_checks", methods={"GET"}) */
    public function listChecks(int $project_id, int $rule_id, RuleRepository $repository): Response
    {
        $rule = $repository->find($rule_id);

        if ($rule->getProject()->getId() !== $project_id) {
            $rule = null;
        }

        if (!$rule) {
            return $this->returnNotFound('Rule was not found');
        }

        return new JsonResponse($this->mapper->checks($rule));
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(string $id, ProjectRepository $repository, EntityManagerInterface $em): Response
    {
        // todo: check whether project belongs to user
        $project = $repository->find($id);
        $em->remove($project);
        $em->flush();

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/", name="create", methods={"POST"})
     */
    public function create(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        try {
            $project = new Project();
            $project->setUrl($data['url']);
            $project->setTitle($data['title']);
            // $project->setUserAgent($data['ua']);
            $project->setInterval($data['interval']);
            $project->setUserAgent($data['ua']);
            $project->addUser($this->security->getUser());

            $rule = new Rule();
            $rule
                ->setUrl('/')
                ->setTitle('Úvodní stránka')
            ;

            $check = new Check();
            $check
                ->setType(Check::TYPE_HTTP_STATUS)
                ->setComparison(Check::COMPARISON_EQUALS)
                ->setExpected(Response::HTTP_OK)
            ;

            $rule->addCheck($check);

            $project->addRule($rule);

            $this->em->persist($check);
            $this->em->persist($rule);
            $this->em->persist($project);
            $this->em->flush();
        } catch (\Exception $e) {
            return $this->returnPreconditionFailed($e->getMessage());
        }

        return $this->returnCreated(['project_id' => $project->getId()]);
    }
}
