<?php

namespace App\Controller\API;

use App\Mapper\UserMapper;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/** @Route("/api/users", name="api_user_") */
final class UserController extends ApiController
{
    /** @var UserMapper */
    private $mapper;

    public function __construct(UserMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /** @Route("/", name="list", methods={"GET"}) */
    public function list(UserRepository $repository): Response
    {
        $users = $repository->findAllActive();

        return new JsonResponse($this->mapper->all($users, $this->getUser()));
    }
}
