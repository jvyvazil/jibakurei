<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class ApiController extends AbstractController
{
    protected function returnNotFound(string $message): JsonResponse
    {
        return new JsonResponse(['message' => $message ?: 'Resource not found'], JsonResponse::HTTP_NOT_FOUND);
    }

    protected function returnCreated(array $headers = []): JsonResponse
    {
        return new JsonResponse('Created', JsonResponse::HTTP_CREATED, $headers);
    }

    protected function returnPreconditionFailed(string $message): JsonResponse
    {
        return new JsonResponse(['message' => $message], JsonResponse::HTTP_PRECONDITION_FAILED);
    }
}
