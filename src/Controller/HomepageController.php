<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Entity\Password;
use App\Form\Entity\Register;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

final class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     *
     * @return Response
     */
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirect('/dashboard');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('homepage/index.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/set-password/{hash}", name="set_password")
     */
    public function setPassword(string $hash, Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder): Response
    {
        if ($this->getUser()) {
            return $this->redirect('/dashboard');
        }

        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneBy(['hash' => $hash, 'password' => null]);

        if (!$user) {
            $this->addFlash('error', 'Uživatel pro nastavení hesla nenalezen, jste si jist, že jste heslo již nenastavil?');

            return $this->redirectToRoute('homepage');
        }

        $password = new Password();
        $form = $this
            ->createFormBuilder($password)
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Hesla se musí shodovat',
                'required' => true,
                'options' => ['attr' => ['class' => 'form-control']],
                'first_options' => ['label' => 'Heslo'],
                'second_options' => ['label' => 'Heslo znovu'],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Nastavit',
                'attr' => ['class' => 'btn btn-primary form-control'],
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->getData();

            $user
                ->clearHash()
                ->setPassword($encoder->encodePassword($user, $password->password));

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Heslo bylo úspěšně nastaveno, můžete se přihlásit');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('homepage/set-password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, EntityManagerInterface $em): Response
    {
        if ($this->getUser()) {
            return $this->redirect('/dashboard');
        }

        if ('true' !== getenv('REGISTRATION_ENABLED')) {
            $this->addFlash('warning', 'Registrace nejsou povoleny');

            return $this->redirectToRoute('homepage');
        }

        $register = new Register();
        $form = $this
            ->createFormBuilder($register)
            ->add('email', EmailType::class, [
                'attr' => ['class' => 'form-control'],
                'required' => true,
                'label' => 'Emailová adresa',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Odeslat',
                'attr' => ['class' => 'btn btn-primary form-control'],
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $register = $form->getData();
            $user = $register->convertToUser();

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Uživatel s emailem '.$user->getEmail().' byl úspěšně zaregistrován, v emailu máte odkaz pro dokončení registrace.');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('homepage/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/forgotten-password", name="forgotten")
     */
    public function forgottenPasword(Request $request, UserRepository $repository, EntityManagerInterface $em): Response
    {
        if ($this->getUser()) {
            return $this->redirect('/dashboard');
        }

        $forgotten = new Register();
        $form = $this
            ->createFormBuilder($forgotten)
            ->add('email', EmailType::class, [
                'attr' => ['class' => 'form-control'],
                'required' => true,
                'label' => 'Emailová adresa',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Odeslat',
                'attr' => ['class' => 'btn btn-primary form-control'],
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $register = $form->getData();
            $email = $register->email;

            $user = $repository->findOneBy(['email' => $email]);

            if (!$user) {
                $this->addFlash('error', 'Zadaný uživatel není v systému zaregistrován');
            } else {
                $user->generateHashWithDatetime();
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Požadavek na změnu hesla byl úspěšně proveden, do emailu vám dorazí odkaz pro změnu hesla.');
            }

            return $this->redirectToRoute('homepage');
        }

        return $this->render('homepage/forgotten.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/change-password/{hash}", name="change_password")
     */
    public function changePassword(string $hash, Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder): Response
    {
        if ($this->getUser()) {
            return $this->redirect('/dashboard');
        }

        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneBy(['hash' => $hash]);

        if (!$user) {
            $this->addFlash('error', 'Nenalezen uživatel pro změnu hesla');

            return $this->redirectToRoute('homepage');
        }

        $now = time();
        $forgottenDate = strtotime($user->getForgottenAt()->format('Y-m-d'));
        $datediff = $now - $forgottenDate;
        $diffInDays = round($datediff / (60 * 60 * 24));

        if ($diffInDays > 14) {
            $this->addFlash('error', 'Heslo je nutné změnit do 14 dní od vyžádání odkazu pro změnu hesla');

            return $this->redirectToRoute('homepage');
        }

        $password = new Password();
        $form = $this
            ->createFormBuilder($password)
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Hesla se musí shodovat',
                'required' => true,
                'options' => ['attr' => ['class' => 'form-control']],
                'first_options' => ['label' => 'Heslo'],
                'second_options' => ['label' => 'Heslo znovu'],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Nastavit',
                'attr' => ['class' => 'btn btn-primary form-control'],
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->getData();

            $user
                ->clearHash()
                ->setPassword($encoder->encodePassword($user, $password->password));

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Heslo bylo úspěšně změněno, můžete se přihlásit');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('homepage/set-password.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
