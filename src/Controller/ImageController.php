<?php

namespace App\Controller;

use Nette\Utils\Image;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

final class ImageController extends AbstractController
{
    /** @var KernelInterface */
    private $kernel;

    /**
     * ImageController constructor.
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Route("/image/{width}/{height}/{filename}", name="show_image")
     *
     * @param mixed $filename
     * @param mixed $width
     * @param mixed $height
     *
     * @throws \Nette\Utils\UnknownImageFileException
     *
     * @return Response
     */
    public function showImage($filename, $width, $height)
    {
        $sourceDir = $this->kernel->getProjectDir().'/public/uploads';
        $img = null;
        $image = null;

        try {
            $image = file_get_contents($this->getFilePath($sourceDir, $filename));
            $img = Image::fromString($image);
        } catch (\Exception $e) {
            $this->createNotFoundException('The requested image was not found');
        }

        if ('0' === $width) {
            $width = (int) ($img->getWidth() * $height / $img->getHeight());
        }

        if ('0' === $height) {
            $height = (int) ($img->getHeight() * $width / $img->getWidth());
        }

        $targetDir = $this->kernel->getProjectDir().'/public/images/'.$width.'/'.$height;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        // Do not resize if original image has same width and height
        if ($img->getWidth() !== $width || $img->getHeight() !== $height) {
            /*
            * Resize to given width and height and add white stripes if original image
            * has different dimension ratio
            */
            $img->resize($width, $height, Image::SHRINK_ONLY | Image::FIT);
            //$image->sharpen();

            $outputImage = Image::fromBlank($width, $height, Image::rgb(255, 255, 255));

            $outputImage->place($img, '50%', '50%');

            $image = $outputImage->toString($img::JPEG);
        }

        file_put_contents($this->getFilePath($targetDir, $filename), $image);

        return new Response($image, Response::HTTP_OK, [
            'Content-Type' => Image::JPEG,
        ]);
    }

    private function getFilePath(string $sourceDir, string $filename)
    {
        return $sourceDir.'/'.substr($filename, 0, 2).'/'.substr($filename, 2);
    }
}
