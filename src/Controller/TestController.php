<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

final class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test_heading")
     */
    public function index(): Response
    {
        return $this->render('test/index.html.twig', [
            'title' => 1 === rand(0, 2) ? 'Funguje' : 'Nefunguje',
        ]);
    }

    /**
     * @Route("/test/404", name="test_404")
     */
    public function fourZeroFour(): Response
    {
        if (1 === rand(0, 2)) {
            throw new NotFoundHttpException('Requested page was not found');
        }

        return $this->render('test/404.html.twig', []);
    }

    /**
     * @Route("/test/500", name="test_500")
     */
    public function fiveZeroZero(): Response
    {
        if (1 === rand(0, 2)) {
            throw new \Exception('Oh, this should not happen');
        }

        return $this->render('test/500.html.twig', []);
    }
}
