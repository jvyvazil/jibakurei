<?php

namespace App\Controller;

use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard", name="dashboard_")
 */
final class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('dashboard/index.html.twig', [
            'title' => 'Přehled projektů',
            'subtitle' => null,
        ]);
    }

    /**
     * @Route("/detail/{id}", name="detail")
     */
    public function detail(int $id, ProjectRepository $repository): Response
    {
        if (!$repository->hasUserAccess($this->getUser(), $id)) {
            throw new UnauthorizedHttpException('You don\'t have access to this project');
        }

        $project = $repository->find($id);

        return $this->render('dashboard/detail.html.twig', [
            'title' => 'Detail projektu',
            'subtitle' => $project->getTitle(),
            'project' => $project->getId(),
        ]);
    }

    /**
     * @Route("/create", name="create")
     */
    public function create(): Response
    {
        return $this->render('dashboard/create.html.twig', [
            'title' => 'Přidat projekt',
            'subtitle' => null,
        ]);
    }
}
