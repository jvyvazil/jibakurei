<?php

namespace App\Enum;

final class UserAgent
{
    public const BOT = 'bot';
    public const FIREFOX = 'firefox';
    public const CHROME = 'chrome';
    public const SAFARI = 'safari';
    public const IE = 'ie';

    private $agents = [
        self::BOT => 'JibakureiBot/1.0 (Linux x86_64; +https://www.jibakurei.cz/jibakureibot) minicrawler/5.1.1',
        self::FIREFOX => 'Mozilla/5.0 (X11; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0',
        self::CHROME => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0 Safari/537.36',
        self::SAFARI => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/600.3.18 (KHTML, like Gecko) Version/8.0 Safari/600.3.18',
        self::IE => 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko',
    ];

    public function getUA(string $key): string
    {
        return $this->agents[$key];
    }

    public function getList(): array
    {
        return $this->agents;
    }
}
