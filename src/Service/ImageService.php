<?php

namespace App\Service;

use App\Model\ImagePath;
use Nette\Utils\Image;

final class ImageService
{
    /** @var string */
    private $path;

    /**
     * ImageService constructor.
     *
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function save(Image $image): string
    {
        $file = $this->getFilenameForImage($image);

        @mkdir($this->path.'/'.$file->getDir(), 0755);

        $image->save($this->path.'/'.$file->getPath(), 90, Image::JPEG);

        return $file->getPath();
    }

    public function getImageFromBase64($base64): Image
    {
        $base64 = str_replace(['_', '-'], ['/', '+'], $base64);

        return Image::fromString(base64_decode($base64, true));
    }

    public function getFromStringAndSave(string $icon)
    {
        $image = Image::fromString($icon);

        return $this->save($image);
    }

    private function getFilenameForImage(Image $image): ImagePath
    {
        $hash = md5($image);

        return new ImagePath(substr($hash, 0, 2), substr($hash, 2).'.jpeg');
    }
}
