<?php

namespace App\Service;

use App\Entity\Project\Project;
use App\Entity\User;
use Twig\Environment;

final class EmailService
{
    /** @var \Swift_Mailer */
    private $mailer;

    /** @var Environment */
    private $templating;

    public function __construct(Environment $templating)
    {
        $transport = new \Swift_SmtpTransport(getenv('MAILER_HOST'), getenv('MAILER_PORT'));
        $transport
            ->setUsername(getenv('MAILER_USER'))
            ->setPassword(getenv('MAILER_PASSWORD'))
        ;

        $this->mailer = new \Swift_Mailer($transport);
        $this->templating = $templating;
    }

    public function sendUserCreatedEmail(User $user): void
    {
        $this->sendEmail(
            'Registrace do služby jibakurei',
            [$user->getEmail()->getValue()],
            $this->templating->render('emails/registered.html.twig', [
                'user' => $user,
            ])
        );
    }

    public function sendForgottenPasswordEmail(User $user): void
    {
        $this->sendEmail(
            'Odkaz na změnu hesla',
            [$user->getEmail()->getValue()],
            $this->templating->render('emails/forgotten.html.twig', [
                'user' => $user,
            ])
        );
    }

    public function sendEmailAboutIncident(Project $project)
    {
        $this->sendEmail(
            'Incident na projektu: '.$project->getTitle(),
            $project->getEmailAddressesOfUsers(),
            $this->templating->render('emails/incident.html.twig', [
                'project' => $project,
                'timestamp' => new \DateTimeImmutable(), // this is used just to fool cache system in Symfony
            ])
        );
    }

    private function sendEmail(string $subject, array $addresses, string $body): void
    {
        $message = new \Swift_Message($subject);

        $message
            ->setFrom('info@jibakurei.cz')
            ->setTo($addresses)
            ->setBody($body, 'text/html')
        ;

        $this->mailer->send($message);
    }
}
