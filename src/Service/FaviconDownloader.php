<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

final class FaviconDownloader
{
    const API_URL = 'http://s2.googleusercontent.com/s2/favicons?domain_url=%s';

    /** @var ImageService */
    private $service;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(ImageService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    public function getFavicon(string $url): ? string
    {
        try {
            $icon = file_get_contents($this->getUrl($url));

            return $this->service->getFromStringAndSave($icon);
        } catch (\ErrorException $e) {
            $this->logger->error($e->getMessage());
        }

        return null;
    }

    private function getUrl(string $url): string
    {
        return sprintf(self::API_URL, $url);
    }
}
