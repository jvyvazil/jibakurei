<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

final class ScreenshotService
{
    const API_URL = 'https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=%s&screenshot=true';

    /** @var ImageService */
    private $service;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(ImageService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    public function create(string $url): ? string
    {
        try {
            $image = file_get_contents($this->getUrl($url));
            $json = json_decode($image, true);

            $img = $this->service->getImageFromBase64($json['screenshot']['data']);
            $filename = $this->service->save($img);

            return $filename;
        } catch (\ErrorException $e) {
            $this->logger->error($e->getMessage());
        }

        return null;
    }

    private function getUrl(string $url): string
    {
        return sprintf(self::API_URL, $url);
    }
}
