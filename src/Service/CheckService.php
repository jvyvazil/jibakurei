<?php

namespace App\Service;

use App\Entity\Project\Check;
use App\Entity\Project\Project;
use App\Entity\Project\Result;
use App\Enum\UserAgent;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;

final class CheckService
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var ProjectRepository */
    private $projectRepository;

    /** @var Client */
    private $client;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(EntityManagerInterface $em, ProjectRepository $repository, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->projectRepository = $repository;
        $this->client = new Client([
            'timeout' => 5.0,
        ]);
        $this->logger = $logger;
    }

    public function updateLastCheck(Project $project): void
    {
        $project->setCheckedAt(new \DateTimeImmutable());
        $this->em->persist($project);
        $this->em->flush();
    }

    /** @return Project[] */
    public function getProjectsToCheck(): array
    {
        $projects = [];
        $now = new \DateTimeImmutable();
        $all = $this
            ->projectRepository
            ->findAll();

        /** @var Project $project */
        foreach ($all as $project) {
            $fails = $project->countConsecutiveFails();

            if ($project->getCheckedAt() < $now->modify('- '.$project->getComputedInterval($fails).' seconds')) {
                $projects[] = $project;
            }
        }

        return $projects;
    }

    public function performChecks(Project $project): void
    {
        $ua = new UserAgent();
        foreach ($project->getRules() as $rule) {
            try {
                $response = $this->client->get($this->assembleUrl($project->getUrl(), $rule->getUrl()), [
                    'headers' => [
                        'User-Agent' => $ua->getUA($project->getUserAgent()),
                    ],
                ]);
            } catch (RequestException $exception) {
                $response = $exception->getResponse();
            }

            foreach ($rule->getChecks() as $check) {
                $result = new Result();
                $result->setCheck($check);

                switch ($check->getType()) {
                    case Check::TYPE_HTTP_STATUS:
                        $code = $response->getStatusCode();
                        $status = $this->compare($code, $check->getComparison(), $check->getExpected());

                        $result
                            ->setStatus($status ? 1 : 0)
                            ->setHttpStatus($code)
                        ;
                        break;
                    case Check::TYPE_CONTENT:
                        $code = $response->getStatusCode();
                        $content = $response->getBody();

                        $status = $this->compare($content, $check->getComparison(), $check->getExpected());

                        $result
                            ->setStatus($status ? 1 : 0)
                            ->setHttpStatus($code)
                            ->setContent($status ? null : $content)
                        ;

                        break;
                    default:
                        $this->logger->error('incorrect type for check: '.$check->getId());

                        return;
                }

                $this->em->persist($result);
            }
        }
        $this->em->flush();
    }

    private function compare(string $value, string $comparison, string $expected): bool
    {
        switch ($comparison) {
            case Check::COMPARISON_EQUALS:
                return $value === $expected;
            case Check::COMPARISON_CONTAINS:
                return false !== strpos($value, $expected);
            case Check::COMPARISON_DOESNT_CONTAIN:
                return false === strpos($value, $expected);
            default:
                return $value === $expected;
        }
    }

    private function assembleUrl(string $first, string $last): string
    {
        return rtrim($first, '/').str_replace('//', '/', '/'.$last);
    }
}
