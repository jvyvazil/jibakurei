import Vue from 'vue';
import Homepage from './Homepage';

new Vue({
    el: '#app',
    components: { Homepage }
});
