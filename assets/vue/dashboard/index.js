import Vue from 'vue';
import axios from 'axios'
import VueAxios from 'vue-axios'
import Dashboard from './Dashboard';
import MenuProjects from './MenuProjects';
import VuejsDialog from 'vuejs-dialog';
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
import Moment from  'vue-moment';
import 'moment/locale/cs';

export const dataBus = new Vue();

Vue.use(VueAxios, axios, VuejsDialog, dataBus, require('vue-moment'));

var content = new Vue({
    el: '#content',
    components: { Dashboard, Moment }
});

var sidebar = new Vue({
    el: '#projects',
    data: {
        title: "Hello",
    },
    methods: {

    },
    computed: {

    },
    components: { MenuProjects }
});
