import Vue from 'vue';
import axios from 'axios'
import VueAxios from 'vue-axios'
import MenuProjects from './MenuProjects';
import Detail from './components/content/Detail'

export const dataBus = new Vue();

Vue.use(VueAxios, axios);

var content = new Vue({
    el: '#content',
    components: { 'project-detail' : Detail }
});

var sidebar = new Vue({
    el: '#projects',
    data: {

    },
    methods: {

    },
    computed: {

    },
    components: { MenuProjects }
});
