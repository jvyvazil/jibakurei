import Vue from 'vue';
import axios from 'axios'
import VueAxios from 'vue-axios'
import MenuProjects from './MenuProjects';
import CreateItem from './components/content/CreateItem'

export const dataBus = new Vue();

Vue.use(VueAxios, axios);

var content = new Vue({
    el: '#content',
    components: { 'create-form' : CreateItem }
});

var sidebar = new Vue({
    el: '#projects',
    data: {

    },
    methods: {

    },
    computed: {

    },
    components: { MenuProjects }
});
