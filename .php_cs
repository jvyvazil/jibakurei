<?php

return PhpCsFixer\Config::create()
    ->setRiskyAllowed(true)
    ->setRules([
        '@Symfony' => true,
        '@Symfony:risky' => true,
        'array_syntax' => ['syntax' => 'short'],
        'combine_consecutive_unsets' => true,
        'general_phpdoc_annotation_remove' => ['expectedException', 'expectedExceptionMessage', 'expectedExceptionMessageRegExp'],
        'heredoc_to_nowdoc' => true,
        'no_extra_consecutive_blank_lines' => ['break', 'continue', 'extra', 'return', 'throw', 'use', 'parenthesis_brace_block', 'square_brace_block', 'curly_brace_block'],
        'no_unreachable_default_argument_value' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'ordered_class_elements' => true,
        'ordered_imports' => true,
        'php_unit_strict' => true,
        'phpdoc_add_missing_param_annotation' => true,
        'phpdoc_order' => true,
        'phpdoc_align' => false,
        'psr4' => true,
        'strict_comparison' => true,
        'strict_param' => true,
        'binary_operator_spaces' => true,
        'lowercase_keywords' => true,
        'no_unused_imports' => true,
        'self_accessor' => false,
    ])
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->exclude('tests/_support')
            ->exclude('tests/_output')
            ->exclude('tests/_data')
            ->in(__DIR__.'/src')
            ->in(__DIR__.'/tests')
    );
